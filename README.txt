Создать .gitlab-ci.yml, состоящий из следующих шагов:
- build – сборка Docker-образов и push в репозиторий с именем CONTAINER_IMAGE:${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}
- release – после успешной сборки всех Docker-образов – у собранных в предыдущем шаге Docker-контейнеров сделать push в репозиторий с тегом :latest
- migrate – создать в Kubernetes job по миграции данных БД, дождаться выполнения 


Результат выполнения задания: .gitlab-ci.yml, а также примеры kubernetes-конфигов и Docker-образов.

*************************

Создать .gitlab-ci.yml, состоящий из следующих шагов:

- build – сборка Docker-образов и push в репозиторий с именем 

- release – после успешной сборки всех Docker-образов – у собранных в предыдущем шаге Docker-контейнеров сделать push в репозиторий с тегом :latest

- migrate – создать в Kubernetes job по миграции данных БД, дождаться выполнения 


###
**************************

- build
	сборка Docker-образов
	push в репозиторий с именем
	CONTAINER_IMAGE:${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}
	
- release	
	после успешной сборки всех Docker-образов
	сделать push в репозиторий с тегом :latest

- migrate
	создать в Kubernetes job по миграции данных БД
	
****
Результат выполнения задания:
	.gitlab-ci.yml
	kubernetes-конфигов
	Docker-образов